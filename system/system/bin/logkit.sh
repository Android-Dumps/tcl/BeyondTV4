#!/bin/bash
CURTIME=`date +%F_%H-%M-%S`
config="$1"
defaultLogcatSize=0
globalStatus="0"
tarpath=""


function InitBootLog(){
    logenable=`getprop persist.sys.normallog`
    boot_completed=`getprop sys.boot_completed`
    if [ x"${logenable}" = x"true" ] && [ x"${boot_completed}" != x"1" ]; then
        setprop sys.collectbootlog.start true
    fi
}

function CollectBootMainLog(){
    logenable=`getprop persist.sys.normallog`
    argtrue='true'
    if [ "${logenable}" = "${argtrue}" ]; then
    /system/bin/logcat -G 5M
    /system/bin/logcat -f /data/tcl_log/tmp/android_boot.txt -r10240 -n 5 -v threadtime
    fi
}
function CollectBootEventLog(){
    logenable=`getprop persist.sys.normallog`
    argtrue='true'
    if [ "${logenable}" = "${argtrue}" ] ; then
    /system/bin/logcat -b events -f /data/tcl_log/tmp/events_boot.txt -r4096 -n 10 -v threadtime
    fi
}

function CollectBootKernelLog(){
  logenable=`getprop persist.sys.normallog`
  argtrue='true'
  if [ "${logenable}" = "${argtrue}" ] ; then
    dmesg > /data/tcl_log/tmp/dmesg_boot.txt
    /system/xbin/klogkitlogcat -f  /data/tcl_log/tmp/kmsg_boot.txt
  fi
}
#================================== COMMON LOG =========================
function InitLog(){
    logenable=`getprop persist.sys.normallog`
    if [ "${logenable}" = "true" ] ; then
        boot_completed=`getprop sys.boot_completed`
        decrypt_delay=0
        while [ x${boot_completed} != x"1" ];do
            sleep 1
            decrypt_delay=`expr $decrypt_delay + 1`
            boot_completed=`getprop sys.boot_completed`
        done
        clear_Log=`getprop persist.sys.clearLog`
        if [ "${clear_Log}" = "true" ] ; then
               CleanLog
        fi
        NORMAL_LOG_PATH=/data/tcl_log/tmp/${CURTIME}/log
        chmod -R 770 /data/tcl_log/
        mkdir -p  ${NORMAL_LOG_PATH}
        chmod -R 770 /data/tcl_log/tmp/${CURTIME}
        setprop sys.logkit.normallog ${NORMAL_LOG_PATH}
        decrypt='false'
        if [ x"${decrypt}" != x"true" ]; then
            setprop ctl.stop GeorgedebugStartCollectBootMainLog
            setprop ctl.stop GeorgedebugStartCollectBootEventLog
            setprop ctl.stop GeorgedebugStartCollectBootKernelLog
            mv /data/tcl_log/tmp/*_boot.txt* ${NORMAL_LOG_PATH}/
            echo "mv cache to normal"
        fi
        startCollectLog
    fi
}

function startCollectLog(){
    startCollectCommonLog
}

function startCollectCommonLog(){
  setprop ctl.start   GeorgedebugLogcatMain
  setprop ctl.start   GeorgedebugLogcatEvent
  setprop ctl.start   GeorgedebugLogcatKernel
  setprop ctl.start   GeorgedebugDetectStorage
}
function waitToCopyBootLog() {
    START=`date +%s`
    local NORMAL_LOG_PATH=$1
    count=0
    while [ $count -lt 10 ] ;do
        count=$((count + 1))
        sleep 1
        usbdir=$(grep -e "/media_rw/" /proc/mounts | awk '{print $2}' | head -1)
        echo "usbdir=$usbdir"
        if [ ! -n "$usbdir" ]; then
          echo "Usb dir null,cannot mv tar to udisk" >/dev/ttyS1
        else
          if [ ! -d $usbdir/tcl_log ]; then
             mkdir -p $usbdir/tcl_log
          fi
          cp ${NORMAL_LOG_PATH}/*_boot.txt* $usbdir/tcl_log/
          sync
          echo " copy boot log  to udisk" >/dev/ttyS1
          break
        fi
    done
    END=`date +%s`
    duration=`expr ${END} - ${START} 2>/dev/null`
    echo "waitToCopyBootLog-exec-${duration} s" >/dev/ttyS1
}
function DetectStorage() {

  dataSize=$(df "/data" | tail -1 | awk '{print $2}')
  dataAvailSize=$(df "/data" | tail -1 | awk '{print $4}')
  RemainSize=$(expr ${dataSize} / 5)
  GSize=$(echo | awk '{printf("%d",1*1024*1024)}')
  if [ ${RemainSize} -lt ${GSize} ] ; then
    RemainSize=$(expr 1 \* 1024 \* 1024)
  fi
  MaxSdcardSize=$(echo | awk '{printf("%d",1*1000*1024)}')
  echo "DetectStorage dataSize ${dataSize} RemainSize ${RemainSize}  dataAvailSize ${dataAvailSize}" >/dev/ttyS1

  MaxLogsize=$(echo | awk '{printf("%d",1*100*1024)}')

  NORMAL_LOG_PATH=$(getprop sys.logkit.normallog)
  needCopyLog=`getprop sys.collectbootlog.start`
  echo "needCopyLog=${needCopyLog} ${MaxLogsize}" >/dev/ttyS1
  if [ "${needCopyLog}" = "true" ]; then
    setprop sys.collectbootlog.start false
    waitToCopyBootLog ${NORMAL_LOG_PATH} &
  fi
  while [ true ]; do
    globalStatus="1"
    dataSize=$(df "/data" | tail -1 | awk '{print $4}')
    echo "DetectStorage--dataSize=${dataSize}--RemainSize=${RemainSize}" >/dev/ttyS1
    logenable=$(getprop persist.sys.normallog)
    if [ "${logenable}" != "true" ]; then
      echo "DetectStorage--Georgedebug-exit" >/dev/ttyS1
      globalStatus="0"
      return
    fi
    # if data storage less than 1GB
    # move tar file to udisk
    #
    if [ ${dataSize} -le ${RemainSize} ] ; then
        setprop ctl.stop   GeorgedebugLogcatMain
        setprop ctl.stop   GeorgedebugLogcatEvent
        setprop ctl.stop   GeorgedebugLogcatKernel
        setprop persist.sys.normallog 2
        echo "DetectStorage-data size below 20%-Georgedebug-exit" |tee /data/tcl_log/tmp/debug.txt >/dev/ttyS1
        du -sh /data/tcl_log/* | tee -a /data/tcl_log/tmp/debug.txt >/dev/ttyS1
        du -sh /data/tcl_log/tmp/* | tee -a /data/tcl_log/tmp/debug.txt >/dev/ttyS1
        setprop ctl.start   GeorgedebugWriteSettings
        globalStatus="0"
        return
    fi

    if [ -d /sdcard/tcl_log ]; then
       sdcardSize=$(du -s "/sdcard/tcl_log" | awk '{print $1}')
       echo "DetectStorage--sdcardSize=${sdcardSize}--MaxSdcardSize=${MaxSdcardSize}" >/dev/ttyS1
       if [ ${sdcardSize} -ge ${MaxSdcardSize} ] ; then
           MoveToDiskOrDelete
       fi
    fi
    twoGSize=$(echo | awk '{printf("%d",2*1024*1024)}')
    LogSize=$(du -s "/data/tcl_log" | awk '{print $1}')
    echo "DetectStorage---LogSize=${LogSize}--MaxLogsize=${MaxLogsize}" >/dev/ttyS1
    if [ ${LogSize} -ge ${twoGSize} ] ; then
          setprop ctl.stop GeorgedebugLogcatMain
          setprop ctl.stop GeorgedebugLogcatEvent
          setprop ctl.stop GeorgedebugLogcatKernel
          setprop persist.sys.normallog 3
          echo "DetectStorage-data size over 2000m-Georgedebug-exit" |tee /data/tcl_log/tmp/debug.txt >/dev/ttyS1
          du -sh /data/tcl_log/* | tee -a /data/tcl_log/tmp/debug.txt >/dev/ttyS1
          du -sh /data/tcl_log/tmp/* | tee -a /data/tcl_log/tmp/debug.txt >/dev/ttyS1
          setprop  ctl.start GeorgedebugWriteSettings
          globalStatus="0"
          return
    fi

    LogSize=$(du -s "/data/tcl_log" | awk '{print $1}')
    echo "DetectStorage--LogSize=${LogSize}--MaxLogsize=${MaxLogsize}" >/dev/ttyS1
     # log size great than max log size we set
    if [ ${LogSize} -ge ${MaxLogsize} ] ; then
         if [ ! -d /sdcard/tcl_log/tmp ]; then
             mkdir -p /sdcard/tcl_log/tmp
         fi
         NORMAL_LOG_PATH=$(getprop sys.logkit.normallog)
         # tar dir01 dir02 dir03 to Andorid_timestamp.tar.gz
         moveAndTarMultiLog ${NORMAL_LOG_PATH}

         # tar android.txt.01 ... android.txt.n to Android_timestamp.tar.gz
         moveAndTarAndroidList ${NORMAL_LOG_PATH}
         moveTarToUdisk
     fi

    logenable=$(getprop persist.sys.normallog)
    if [ "${logenable}" != "true" ]; then
      echo "DetectStorage-mv--exit" >/dev/ttyS1
      return
    fi
    echo "DetectStorage--sleep 300 s--${globalStatus}" >/dev/ttyS1
    globalStatus="0"
    sleep 300
  done
}
function moveTarToUdisk() {
  if [ -d /sdcard/tcl_log ]; then
        usbdir=$(grep -e "/media_rw/" /proc/mounts | awk '{print $2}' | head -1)
        echo "usbdir=$usbdir"
        if [ ! -n "$usbdir" ]; then
          echo "Usb dir null,cannot mv tar to udisk" >/dev/ttyS1
        else

          if [ ! -d $usbdir/tcl_log ]; then
              mkdir -p $usbdir/tcl_log
          fi
          ALL_FILE=$(ls -tr /sdcard/tcl_log/Android_*.tar.gz)
          for path in $ALL_FILE; do
             usbAvailSize=$(df "$usbdir" | tail -1 | awk '{print $4}')
             KSize=$(ls -l ${path} | awk '{print $5}')
             TarSize=$(expr ${KSize} / 1024)
             echo "moveTarToSdcard-usbAvailSize=${usbAvailSize}-TarSize=${TarSize}" >/dev/ttyS1
             if [ ${usbAvailSize} -ge ${TarSize} ]; then
                  mv ${path} $usbdir/tcl_log/
                  sync
                  echo "-mv-${path}--$usbdir/tcl_log/" >/dev/ttyS1
             else
               echo "udisk storage is not enough " >/dev/ttyS1
               break
             fi
          done
        fi
  fi
}
function checkUdiskAndCopy() {
    local sourceFile=$1
    local usbdir=$2
    echo "checkUdiskAndCopy parameter ${sourceFile} ${usbdir}"
    START=`date +%s`
    usbAvailSize=$(df "$usbdir" | tail -1 | awk '{print $4}')
    sourceSize=$(du -s ${sourceFile} | awk '{print $1}')
    echo "sourceSize=${sourceSize}-usbAvailSize=$usbAvailSize" >/dev/ttyS1
    if [ ${usbAvailSize} -ge ${sourceSize} ]; then
       mv ${sourceFile}  $usbdir/tcl_log/
       sync
       echo "-move--${sourceFile}--${usbdir}/tcl_log/" >/dev/ttyS1
    else
        count=`ls -t $usbdir/tcl_log/Android_*.tar.gz | wc -l`
        echo  "${count}">/dev/ttyS1
        while [ $count -gt 0 ] && [ ${usbAvailSize} -lt ${sourceSize} ];do
              lastUsbFile=`ls -tr $usbdir/tcl_log/Android_*.tar.gz | head -1`
              echo ""> ${lastUsbFile}
              rm -rf ${lastUsbFile}
              sync
              count=$((count - 1))
              echo "delelte--${lastUsbFile}" >/dev/ttyS1
              usbdir=$(grep -e "/media_rw/" /proc/mounts | awk '{print $2}' | head -1)
              if [ ! -n "$usbdir" ]; then
                rm -rf ${sourceFile}
                echo "Usb dir null--delete-sdcard last file ${sourceFile}" >/dev/ttyS1
                return
              fi
              usbAvailSize=$(df "$usbdir" | tail -1 | awk '{print $4}')
              echo "usbAvailSize=${usbAvailSize} sourceSize=&{sourceSize}" >/dev/ttyS1
              if [ ${usbAvailSize} -ge ${sourceSize} ]; then
                mv ${sourceFile}  $usbdir/tcl_log/
                sync
                echo "storage enough-move--${sourceFile}--${usbdir}/tcl_log/ return" >/dev/ttyS1
                return
              fi
              continue
        done
        rm -rf ${sourceFile}
        echo "Usb dir-full-delete-sdcard last file ${sourceFile}" >/dev/ttyS1
      fi
    END=`date +%s`
    duration=`expr ${END} - ${START} 2>/dev/null`
    echo "checkUdiskAndCopy-exec-${duration} s" >/dev/ttyS1
}

function MoveToDiskOrDelete() {
  echo "enter-MoveToDiskOrDelete--" >/dev/ttyS1
  if [  -d /sdcard/tcl_log/ ]; then
     androidCount=`ls -t /sdcard/tcl_log/Android_*.tar.gz | wc -l`
     echo  "/sdcard/tcl_log has ${androidCount} tar.gz files">/dev/ttyS1
     if [ ${androidCount} -gt 0 ];then
        lastFile=`ls -tr /sdcard/tcl_log/Android_*.tar.gz|head -1`
        echo  "${lastFile} ">/dev/ttyS1
        usbdir=$(grep -e "/media_rw/" /proc/mounts | awk '{print $2}' | head -1)
        if [ ! -n "$usbdir" ]; then
           rm -r ${lastFile}
           echo "Usb dir null,delete ${lastFile}" >/dev/ttyS1
        else
           if [ ! -d $usbdir/tcl_log ]; then
              mkdir -p  $usbdir/tcl_log/
           fi
           checkUdiskAndCopy  ${lastFile} ${usbdir}
        fi
     fi
  fi
}
function moveAndTarMultiLog() {
  local NORMAL_LOG_PATH=$1
  START=`date +%s`
  logPath=$(echo -e "${NORMAL_LOG_PATH}" | cut -d "\/" -f5)
  tarpath=""
  count=$(ls -t /data/tcl_log/tmp/ | wc -l)
  echo "moveAndTarMultiLog--/data/tcl_log/tmp/ has ${count} dirs logPath=${logPath}" >/dev/ttyS1
  if [ $count -gt 1 ]; then
     sum=0
     cd /data/tcl_log/tmp/
     ALL_FILE=$(ls -tr /data/tcl_log/tmp/)
     for path in $ALL_FILE; do

        if [ "${path}" == "${logPath}" ];then
           echo "current log write path  ${path}" >/dev/ttyS1
           continue
        fi
        if [ ! -d ${path} ];then
           echo "is not dir  path  ${path}" >/dev/ttyS1
        fi

        fileSize=$(du -s $path | awk '{print $1}')
        sum=$(expr ${sum} + ${fileSize})
        echo "add  ${path} ${fileSize} to sum=${sum}" >/dev/ttyS1
        if [ "${tarpath}" == "" ];then
           tarpath="${path}"
        else
          MaxLogsize=$(echo | awk '{printf("%d",1*100*1024)}')
          if [ ${sum} -gt ${MaxLogsize} ]; then
              echo "log size over 100m path  ${path}" >/dev/ttyS1
              break
          fi
           tarpath="${tarpath} ${path}"
        fi
     done
      echo "tarpath ${tarpath}" >/dev/ttyS1
  fi
   END=`date +%s`
   duration=`expr ${END} - ${START} 2>/dev/null`

   echo "moveAndTarMultiLog-exec-${duration} s" >/dev/ttyS1
}
function moveAndTarAndroidList() {

   local NORMAL_LOG_PATH=$1
   count=$(ls -t ${NORMAL_LOG_PATH}/*.txt* | wc -l)
   echo "moveAndTarAndroidList ${NORMAL_LOG_PATH} has ${count}  *.txt" >/dev/ttyS1
   logPath=$(echo -e "${NORMAL_LOG_PATH}" | cut -d "\/" -f5)
   START=`date +%s`
   if [ $count -ge 1 ]; then
      currenttime=$(date +%F_%H-%M-%S)
      echo "beofore-mv-to sdcard--${currenttime}" >/dev/ttyS1
      cd ${NORMAL_LOG_PATH}
      ALL_FILE=$(ls -t *.txt*)
      echo "ALL_FILE ${ALL_FILE}" >/dev/ttyS1
      setprop ctl.stop  GeorgedebugLogcatKernel
      setprop ctl.stop  GeorgedebugLogcatMain
      setprop ctl.stop  GeorgedebugLogcatEvent

      setprop ctl.start  GeorgedebugLogcatMain
      setprop ctl.start  GeorgedebugLogcatEvent
      setprop ctl.start  GeorgedebugLogcatKernel
      if [ ! -d /sdcard/tcl_log/tmp ]; then
         mkdir -p /sdcard/tcl_log/tmp
      fi
      for path in $ALL_FILE; do
         tarpath="${tarpath} ${logPath}/log/${path}"
      done
   fi
   echo "tarpath ${tarpath}" >/dev/ttyS1
   cd /data/tcl_log/tmp/
   tarCount=$(find -name *.tar.gz 2>/dev/null| wc -l)
   if [ $tarCount -ge 1 ]; then
     echo "beofore-has tarCount tar.gz ${tarCount}" >/dev/ttyS1
     find -name *.tar.gz 2>/dev/null|xargs rm -fr
   fi
   currenttime=$(date +%F_%H-%M-%S)
   tar -zcvf ${NORMAL_LOG_PATH}/Android_${currenttime}.tar.gz ${tarpath}
   mv ${NORMAL_LOG_PATH}/Android_${currenttime}.tar.gz /sdcard/tcl_log/
   rm -fr ${tarpath}
   echo "make a new tar file to  /sdcard/tcl_log/Android_${currenttime}.tar.gz" >/dev/ttyS1
    END=`date +%s`
    duration=`expr ${END} - ${START} 2>/dev/null`
    echo "moveAndTarAndroidList-exec-${duration} s" >/dev/ttyS1
}

function LogcatMain(){
    logenable=`getprop persist.sys.normallog`
    NORMAL_LOG_PATH=`getprop  sys.logkit.normallog`
    if [ "${logenable}" = "true" ] ; then
        logdsize=`getprop persist.logd.size`
        if [ "${logdsize}" = "" ]; then
            /system/bin/logcat -G 5M
        fi
        currenttime=$(date +%F_%H-%M-%S)
        /system/bin/logcat -f ${NORMAL_LOG_PATH}/android_${currenttime}.txt -v threadtime
    else
        setprop ctl.stop GeorgedebugLogcatMain
    fi
}

function LogcatEvent(){
    logenable=`getprop persist.sys.normallog`
    NORMAL_LOG_PATH=`getprop  sys.logkit.normallog`
    if [ "${logenable}" = "true" ] ; then
        currenttime=$(date +%F_%H-%M-%S)
        /system/bin/logcat -b events -f ${NORMAL_LOG_PATH}/events_${currenttime}.txt  -v threadtime
    else
        setprop ctl.stop GeorgedebugLogcatEvent
    fi
}

function LogcatKernel(){
    logenable=`getprop persist.sys.normallog`
    NORMAL_LOG_PATH=`getprop sys.logkit.normallog`
    if [ "${logenable}" = "true" ]; then
       currenttime=$(date +%F_%H-%M-%S)
       /system/xbin/klogkitlogcat -f  ${NORMAL_LOG_PATH}/kmsg_${currenttime}.txt
    fi
}

function MvLogtoSDcard(){
    copyLog
    resetEnvironment
}
function resetEnvironment() {
    bufferSize=`logcat -g|grep "main"| awk '{print $5$6}'`
    if [ "${bufferSize}" == "5MiB" ]; then
       logcat -G 256kb
    fi
}
function copyNormalLog(){
    if [ -d /data/tcl_log/tmp ]; then
       STOP_LOG_PATH=`getprop sys.logkit.stoptime`
       mkdir -p  /data/tcl_log/${STOP_LOG_PATH}
       chmod -R 777 /data/tcl_log/${STOP_LOG_PATH}
       mv /data/tcl_log/tmp/*  /data/tcl_log/${STOP_LOG_PATH}
       setprop sys.tranfer.finished "copyNormalLog"
    fi
}
function copyAnr(){
    if [ -d /data/tcl_log/tmp ]; then
       mkdir -p  /data/tcl_log/tmp/anr
       chmod -R 770 /data/tcl_log/tmp/anr
       cp  /data/anr/*  /data/tcl_log/tmp/anr
       touch /data/tcl_log/tmp/copy_anr
       setprop sys.tranfer.finished "copyAnr"
    fi
}
function copyTombstone(){
    if [ -d /data/tcl_log/tmp ]; then
       mkdir -p /data/tcl_log/tmp/tombstones
       chmod -R 770 /data/tcl_log/tmp/tombstones
       mv /data/tombstones/tombstone* /data/tcl_log/tmp/tombstones
       setprop sys.tranfer.finished "copyTombstone"
    fi
}
function copyPstore(){
  if [ -d /data/tcl_log/tmp ]; then
    cp -a /sys/fs/pstore/* /data/tcl_log/tmp/
    chmod 770 /data/tcl_log/tmp/*
    touch /data/tcl_log/tmp/copy_pstore
    setprop sys.tranfer.finished "copyPstore"
  fi
}
function copyCoredump(){
    setprop sys.tranfer.finished "copyCoredump"
}
function waitToCopyAnr() {
    rm -fr /data/tcl_log/tmp/copy_anr
    setprop ctl.start GeorgedebugCopyAnr
    count=0
    while [ $count -le 20 ] && [ ! -f /data/tcl_log/tmp/copy_anr ];do
          count=$((count + 1))
          sleep 1
    done
    rm -fr /data/tcl_log/tmp/copy_anr
}
function waitToDumpSystem() {
    setprop ctl.start dumpSystem
    count=0
    while [ $count -le 60 ] && [ ! -f /data/tcl_log/tmp/finish_system ];do
        count=$((count + 1))
        sleep 1
    done
    rm -fr data/tcl_log/tmp/finish_system
}
function waitToCopyPstone() {
    rm -fr /data/tcl_log/tmp/copy_pstore
    setprop ctl.start   GeorgedebugCopyPstore
    count=0
    while [ $count -le 5 ] && [ ! -f /data/tcl_log/tmp/copy_pstore ];do
        count=$((count + 1))
        sleep 1
    done
    rm -fr /data/tcl_log/tmp/copy_pstore
}

function CopyBlueToothLog(){
  if [ -d /data/tcl_log/tmp ]; then
    mkdir -p /data/tcl_log/tmp/bluetooth
    chmod -R 770 /data/tcl_log/tmp/bluetooth
    cp -a /data/misc/bluedroid/* /data/tcl_log/tmp/bluetooth
    chmod -R 777 /data/tcl_log/tmp/*
    touch /data/tcl_log/tmp/copy_bluetooth
    setprop sys.tranfer.finished "CopyBlueToothLog"
  fi
}

function WaitToCopyBlueToothLog() {

    rm -fr /data/tcl_log/tmp/copy_bluetooth
    setprop ctl.start   GeorgedebugCopyBluetoothLog
    count=0
    while [ $count -le 20 ] && [ ! -f /data/tcl_log/tmp/copy_bluetooth ];do
        count=$((count + 1))
        sleep 1
    done
    rm -fr /data/tcl_log/tmp/copy_bluetooth
    #setprop ctl.start   GeorgedebugKillBluetooth
}
function WaitToDetectStorage() {

  count=0
  setprop sys.tranfer.finished "WaitToDetectStorage"
  while [ $count -le 120 ] ; do
    count=$((count + 1))
    echo "${globalStatus}">/dev/ttyS1
    if [ "${globalStatus}" = "0" ]; then
       echo "stop GeorgedebugDetectStorage">/dev/ttyS1
       setprop ctl.stop GeorgedebugDetectStorage
       break
    fi
    sleep 1
  done
}
function KillBluetooth() {
    bluetoothpid=`ps -A | grep com.android.bluetooth|grep -v remote | head -n 1| awk '{print $2}'`
    if [ x"${bluetoothpid}" != x"" ]; then
       kill -9 $bluetoothpid
    fi
}
function copyLog(){
    waitToDumpSystem
    waitToCopyAnr
    waitToCopyPstone
    waitToCatKfence
    WaitToCopyBlueToothLog
    copyTombstone
    copyCoredump
    copyNormalLog
    STOP_LOG_PATH=`getprop sys.logkit.stoptime`
    chown root:system -R /data/tcl_log/*
    chmod -R 770 /data/tcl_log/*

    MvtoUdisk  ${STOP_LOG_PATH}
    WaitToDetectStorage
    setprop sys.tranfer.finished "1"
}
function MvtoUdisk(){
    local STOP_LOG_PATH=$1
    MvtoSdcard
    usbdir=`grep -e "/media_rw/" /proc/mounts | awk '{print $2}' | head -1`
    echo "usbdir=$usbdir"
    if [ ! -n "$usbdir" ]; then
       setprop sys.tranfer.finished "Usb dir null, Move to sdcard"
    else
       usbAvailSize=`df "$usbdir" |  tail -1 | awk '{print $4}'`
       FreeSize=`du -s /sdcard/tcl_log/ | awk '{print $1}'`
       if [ ${usbAvailSize} -ge  ${FreeSize} ];then
           if [ ! -d  $usbdir/tcl_log ];then
              mkdir -p $usbdir/tcl_log
           fi
           mv /sdcard/tcl_log/Android_*.tar.gz $usbdir/tcl_log
           sync
           setprop sys.tranfer.finished "Move to udisk"
       else
           setprop sys.tranfer.finished "Usb dir full,Move to sdcard"
       fi
    fi
    sleep 1
}


function PerfettoStart(){
    perfetto --out  /data/misc/perfetto-traces/trace.txt  sched/sched_switch sched/sched_wakeup sched/sched_wakeup_new sched/sched_waking sched/sched_process_exit sched/sched_process_free ftrace/print gfx input view webview wm am sm hal res dalvik rs bionic power pm ss network binder_driver binder_lock
    #atrace -z -b 32769 -t 10 gfx input view webview wm am sm hal res dalvik rs bionic power pm ss network >/data/tcl_log/tmp/trace1.txt
}
function mvPerfetto(){
    #setprop persist.traced.enable 0
    #cat /data/misc/perfetto-traces/trace > /data/tcl_log/trace.perfetto-trace
    #mv /data/system/dropbox/perfetto*  /data/tcl_log/
    /system/bin/cp /data/misc/perfetto-traces/trace.txt /data/tcl_log/tmp/
    /system/bin/chmod 770 /data/tcl_log/tmp/trace.txt
    if [ ! -d  /sdcard/tcl_log/ ];then
        mkdir  -p /sdcard/tcl_log
    fi
    mv /data/tcl_log/tmp/trace.txt /sdcard/tcl_log/
}
function getSystemStatus() {
    boot_completed=`getprop sys.boot_completed`
    if [ -d /data/tcl_log/tmp ]; then
    outputPath=/data/tcl_log/tmp
    rm -fr /data/tcl_log/tmp/finish_system
    dumpsys -t 20 meminfo > ${outputPath}/dumpsys_mem.txt &
    setprop sys.tranfer.finished dumpsysmeminfo
    ps -T -A > ${outputPath}/ps.txt
    setprop sys.tranfer.finished ps
    export COLUMNS=180
    top -n 2 -s 10 > ${outputPath}/top.txt
    export COLUMNS=80
    setprop sys.tranfer.finished top
    cat /proc/meminfo > ${outputPath}/proc_meminfo.txt
    setprop sys.tranfer.finished procmeminfo
    getprop > ${outputPath}/prop.txt
    setprop sys.tranfer.finished getprop
    df > ${outputPath}/df.txt
    setprop sys.tranfer.finished df
    mkdir -p  ${outputPath}/dropbox
    chmod -R 770 ${outputPath}/dropbox
    #mv /data/system/dropbox/*  ${outputPath}/dropbox/
    dumpsys dropbox --print > ${outputPath}/dropbox/dumpsys_dropbox_all.txt
    setprop sys.tranfer.finished dropbox
    if [ -f "/dev/binderfs/binder_logs/state" ]; then
        cat /dev/binderfs/binder_logs/state > ${outputPath}/binder_state.txt
    else
        cat /d/binder/state > ${outputPath}/binder_state.txt
    fi
    setprop sys.tranfer.finished binder
    wait
    touch /data/tcl_log/tmp/finish_system
    fi
}
function CleanLog() {
    if [ -d /data/tcl_log/ ]; then
      ALL_FILE=`ls -t /data/tcl_log/`
      for path in $ALL_FILE;
      do
          if [ $path != "tmp" ] ; then
              rm -fr  /data/tcl_log/$path
          fi
      done
    fi
    if [ -d /sdcard/tcl_log/ ]; then
       rm -fr  /sdcard/tcl_log/[0-9]*
    fi
}
function ClearAllLog() {
  setprop sys.clear.finish "clear data log"
  chmod -R 777 /data/tcl_log/*
  chown root:system -R /data/tcl_log/*
  if [ -d /data/tcl_log/ ]; then
    ALL_FILE=$(ls -t /data/tcl_log/)
    for path in $ALL_FILE; do
      if [ $path != "tmp" ]; then
        rm -fr /data/tcl_log/$path
      else
        cd /data/tcl_log/tmp/
        rm -fr *
      fi
    done
  fi
  sleep 1
  setprop sys.clear.finish "clear sdcard log"
  if [ -d /sdcard/tcl_log/ ]; then
    rm -fr /sdcard/tcl_log/*
  fi
  sleep 1
  setprop sys.clear.finish "clear usb log"
  usbdir=$(grep -e "/media_rw/" /proc/mounts | awk '{print $2}' | head -1)
  echo "usbdir=$usbdir"
  if [ ! -n "$usbdir" ]; then
    echo "Usb dir null, Move to sdcard"
  else
    if [ -d $usbdir/tcl_log ]; then
       cd $usbdir/tcl_log
       rm -fr  $usbdir/tcl_log/*
    fi
  fi
  sleep 1
  setprop sys.clear.finish "1"
}
function DumpHprof() {
  PACKAGE_NAME=`getprop sys.logkit.package`
  if [ "${PACKAGE_NAME}" != "" ]; then
    am dumpheap ${PACKAGE_NAME} /data/local/tmp/heapdump_${PACKAGE_NAME}.hprof
    /system/bin/chmod 770 /data/local/tmp/heapdump*
  fi
}

function StartDebugWifi() {
  if [ -f "/proc/net/wlan/dbg_level" ]; then
   echo 0xff:0x3f > /proc/net/wlan/dbg_level
  fi
  if [ -f "/proc/net/wlan/dbgLevel" ]; then
   echo 0xff:0x3f > /proc/net/wlan/dbgLevel
  fi
  if [ -f "/proc/net/rtl8192fu/log_level" ]; then
    echo 6 > /proc/net/rtl8192fu/log_level
  fi
  if [ -f "/proc/net/rtl8192eu/log_level" ]; then
    echo 6 > /proc/net/rtl8192eu/log_level
  fi
  Version=`getprop ro.build.version.release`
  if [ "${Version}" == "9" ]; then
     svc wifi logon
  fi
  if [ "${Version}" == "11" ]; then
     cmd wifi set-verbose-logging enabled
  fi
}
function StartIwpriv() {
  if [ -f "/proc/net/wlan/dbg_level" ]; then
   iwpriv wlan0 driver "set_fwlog 0 2"
   iwpriv wlan0 driver ver
  fi
  if [ -f "/proc/net/wlan/dbgLevel" ]; then
   iwpriv wlan0 driver "set_fwlog 0 2"
   iwpriv wlan0 driver ver
  fi
}
function StopDebugWifi() {
  if [ -f "/proc/net/wlan/dbg_level" ]; then
   echo 0xff:0x0f > /proc/net/wlan/dbg_level
  fi
  if [ -f "/proc/net/wlan/dbgLevel" ]; then
   echo 0xff:0x0f > /proc/net/wlan/dbgLevel
  fi
  if [ -f "/proc/net/rtl8192fu/log_level" ]; then
    echo 2 > /proc/net/rtl8192fu/log_level
  fi
  if [ -f "/proc/net/rtl8192eu/log_level" ]; then
    echo 4 > /proc/net/rtl8192eu/log_level
  fi
  Version=`getprop ro.build.version.release`
  if [ "${Version}" == "9" ]; then
     svc wifi logoff
  fi
  if [ "${Version}" == "11" ]; then
     cmd wifi set-verbose-logging disabled
  fi
}
function StartKfence() {
  if [ -f "/proc/kfence_dyn_alloc" ]; then
   echo 1 > /proc/kfence_dyn_alloc
  fi
}
function waitToCatKfence() {
  if [ -f "/proc/kfence_dyn_alloc" ]; then
     if [ -d /data/tcl_log/tmp ]; then
       cat /proc/kfence/objects > /data/tcl_log/tmp/kfence_objects.txt
       cat /proc/kfence/stats > /data/tcl_log/tmp/kfence_stats.txt
     fi
  fi
}
function CatKfence() {
  if [ -f "/proc/kfence_dyn_alloc" ]; then
     if [ ! -d  /sdcard/tcl_log/ ];then
        mkdir  -p /sdcard/tcl_log
     fi
     if [ -d /sdcard/tcl_log ]; then
       cat /proc/kfence/objects > /sdcard/tcl_log/kfence_objects.txt
       cat /proc/kfence/stats > /sdcard/tcl_log/kfence_stats.txt
     fi
  fi
}
function EnablePrintk() {
  PRINTK_TYPE=`getprop sys.logkit.printk`
  if [ "${PRINTK_TYPE}" != "" ]; then
    echo ${PRINTK_TYPE} > /proc/sys/kernel/printk
  fi
}
function MvtoSdcard() {
  if [ -d /sdcard/ ];then
     if [ ! -d  /sdcard/tcl_log/ ];then
        mkdir  -p /sdcard/tcl_log
     fi
    START=`date +%s%N`
    currenttime=$(date +%F_%H-%M-%S)
    cd /data/tcl_log/
    tar -zcvf /data/tcl_log/Android_${currenttime}.tar.gz [0-9]*
    mv /data/tcl_log/Android_${currenttime}.tar.gz /sdcard/tcl_log
    rm -fr /data/tcl_log/[0-9]*
    END=`date +%s%N`
    duration=`expr ${END} - ${START} 2>/dev/null`
    echo "MvtoSdcard-exec-${duration} s" >/dev/ttyS1
  fi
}
function MakeTarFile() {
  setprop sys.tar.finish ""
  if [ -d /sdcard/tcl_log ];then
     cd /sdcard/tcl_log
    androidCount=`ls -t /sdcard/tcl_log/Android_*.tar.gz | wc -l`
    if [ ${androidCount} -gt 0 ];then
       if [ -d /sdcard/Android/data/com.tcl.logkit/files/ ];then
         mv Android_*.tar.gz /sdcard/Android/data/com.tcl.logkit/files/
       fi
    fi
  fi
  setprop sys.tar.finish "1"
}
function mvPerformance() {
     if [ ! -d  /sdcard/tcl_log/ ];then
        mkdir  -p /sdcard/tcl_log/
     fi

     if [ -d /sdcard/Android/data/com.tcl.logkit/files/ ];then
        cd /sdcard/Android/data/com.tcl.logkit/files/
        if [ -f memLeak.txt ];then
           mv memLeak.txt /sdcard/tcl_log/
        fi
        if [ -f meminfo.txt ];then
           mv meminfo.txt /sdcard/tcl_log/
        fi
        if [ -f performance.csv ];then
           mv performance.csv /sdcard/tcl_log/
        fi
        if [ -f CpuInfo.txt ];then
           mv CpuInfo.txt /sdcard/tcl_log/
        fi
     fi
     mv /data/local/tmp/heapdump* /sdcard/tcl_log/
}
function DumpMiddleWare() {
    if [ -f /vendor/tvos/bin/tcli ];then
       export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/vendor/lib:/system/lib:/system/apex/com.android.art.release/lib:/vendor/tvos/libBionic
       MIDDLEWARE_TYPE=`getprop sys.logkit.middlewaredebugtype`
       if [ "${MIDDLEWARE_TYPE}" != "" ]; then
          /vendor/tvos/bin/tcli ${MIDDLEWARE_TYPE}
       fi
    fi
}
function WriteSettings() {
    dataSize=$(df "/data" | tail -1 | awk '{print $4}')
    settings put global logkit_observer_size "${dataSize}"
}


case "$config" in
    "InitBootLog")
     InitBootLog
     ;;
     "InitLog")
      InitLog
     ;;
    "CollectBootMainLog")
     CollectBootMainLog
     ;;
     "CollectBootEventLog")
     CollectBootEventLog
     ;;
     "CollectBootKernelLog")
     CollectBootKernelLog
     ;;
     "LogcatMain")
     LogcatMain
     ;;
     "LogcatEvent")
     LogcatEvent
     ;;
     "LogcatKernel")
     LogcatKernel
     ;;
     "MvLogtoSDcard")
     MvLogtoSDcard
     ;;
     "PerfettoStart")
     PerfettoStart
     ;;
     "mvPerfetto")
     mvPerfetto
     ;;
     "dumpSystem")
      getSystemStatus
     ;;
     "resetEnvironment")
      resetEnvironment
     ;;
     "CopyAnr")
      copyAnr
     ;;
     "copyPstore")
      copyPstore
     ;;
     "mvPerformance")
      mvPerformance
     ;;
     "CopyBlueToothLog")
      CopyBlueToothLog
     ;;
     "KillBluetooth")
      KillBluetooth
     ;;
     "CleanLog")
      CleanLog
     ;;
     "DumpHprof")
      DumpHprof
     ;;
    "StartDebugWifi")
      StartDebugWifi
     ;;
   "StartIwpriv")
      StartIwpriv
     ;;
    "StopDebugWifi")
      StopDebugWifi
     ;;
   "StartKfence")
      StartKfence
     ;;
   "EnablePrintk")
      EnablePrintk
     ;;
   "MvtoUdisk")
      MvtoUdisk
     ;;
   "MakeTarFile")
      MakeTarFile
     ;;
   "CatKfence")
      CatKfence
     ;;
   "DumpMiddleWare")
      DumpMiddleWare
     ;;
   "DetectStorage")
    DetectStorage
    ;;
    "ClearAllLog")
     ClearAllLog
    ;;
  "WriteSettings")
     WriteSettings
    ;;
     *)
     ;;
esac
