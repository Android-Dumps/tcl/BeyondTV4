#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_BeyondTV4.mk

COMMON_LUNCH_CHOICES := \
    omni_BeyondTV4-user \
    omni_BeyondTV4-userdebug \
    omni_BeyondTV4-eng
