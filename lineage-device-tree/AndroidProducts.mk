#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_BeyondTV4.mk

COMMON_LUNCH_CHOICES := \
    lineage_BeyondTV4-user \
    lineage_BeyondTV4-userdebug \
    lineage_BeyondTV4-eng
