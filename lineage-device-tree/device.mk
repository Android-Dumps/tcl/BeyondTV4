#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Health
PRODUCT_PACKAGES += \
    android.hardware.health@2.1-impl \
    android.hardware.health@2.1-impl.recovery \
    android.hardware.health@2.1-service

# Overlays
PRODUCT_ENFORCE_RRO_TARGETS := *

# Product characteristics
PRODUCT_CHARACTERISTICS := nosdcard,tv

# Rootdir
PRODUCT_PACKAGES += \
    btsnoop_enable.sh \
    install-recovery.sh \

PRODUCT_PACKAGES += \
    fstab.rtd288o \
    init.rtd288o.rc \
    init.rtd288o.usb.rc \
    tclinit.rc \
    init.rtd288o.skd.rc \
    init.recovery.rtd288o.rc \

# Shipping API level
PRODUCT_SHIPPING_API_LEVEL := 28

# Soong namespaces
PRODUCT_SOONG_NAMESPACES += \
    $(LOCAL_PATH)

# Inherit the proprietary files
$(call inherit-product, vendor/tcl/BeyondTV4/BeyondTV4-vendor.mk)
