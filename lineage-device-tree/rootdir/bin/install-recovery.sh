#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/mmcblk0p3:33554432:2d81b9de13f7e324cc891a38708c6df63eec92af; then
  applypatch \
          --flash /vendor/etc/recovery.img \
          --target EMMC:/dev/block/mmcblk0p3:33554432:2d81b9de13f7e324cc891a38708c6df63eec92af && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
