#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from BeyondTV4 device
$(call inherit-product, device/tcl/BeyondTV4/device.mk)

PRODUCT_DEVICE := BeyondTV4
PRODUCT_NAME := lineage_BeyondTV4
PRODUCT_BRAND := TCL
PRODUCT_MODEL := Smart TV Pro
PRODUCT_MANUFACTURER := tcl

PRODUCT_GMS_CLIENTID_BASE := android-tcl-tv

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="BeyondTV4-user 11 RP1A.200622.001 AR06 release-keys"

BUILD_FINGERPRINT := TCL/BeyondTV6/BeyondTV4:11/RP1A.200622.001/AR06:user/release-keys
