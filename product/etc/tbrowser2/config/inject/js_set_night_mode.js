TUtil.println("---add js_set_night_mode.js-- start-");

function setDivsStyle() {
	var allDivs = document.getElementsByTagName('div');
	var length = allDivs.length;
	var i;
	for(i = 0; i < length; i++) {
		allDivs[i].style.backgroundColor = "#000000";
		allDivs[i].style.color = "#FFFFFF";
	}
}

function setTablesStyle() {
	var allTables = document.getElementsByTagName('table');
	var length = allTables.length;
	var i;
	for(i = 0; i < length; i++) {
		allTables[i].style.backgroundColor = "#000000";
	}
}

function setTdsStyle() {
	var allTds = document.getElementsByTagName('td');
	var length = allTds.length;
	var i;
	for(i = 0; i < length; i++) {
		allTds[i].style.backgroundColor = "#000000";
	}
}

function setAsStyle() {
	var allAs = document.getElementsByTagName('a');
	var length = allAs.length;
	var i;
	for(i = 0; i < length; i++) {
		allAs[i].style.color = "#FFFFFF";
	}
}
function setElementFontColor(node,color) {
	var allNodes = document.getElementsByTagName(node);
	var length = allNodes.length;
	var i ;
	console.log("caosm---"+node+",color--"+color);
	for(i = 0; i < length; i++) {
		allNodes[i].style.color = color;
	}
	
}

setDivsStyle();
setTablesStyle();
setTdsStyle();
setAsStyle();

//setElementFontColor('a',"#FFFFFF"); 

TUtil.println("---add js_set_night_mode.js-- end-");

