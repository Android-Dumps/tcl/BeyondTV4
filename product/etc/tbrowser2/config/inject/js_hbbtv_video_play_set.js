console.log("-- inject js_hbbtv_video_play_set.js---");

function overrideHTMLElementFunctionHook(target, fun, callbackFunction) {
    let origFunction = target[fun];
    target[fun] = function wrapper() {
        var ret  = callbackFunction.apply(this, arguments);
        if (ret) {
            var res = origFunction.apply(this, arguments);
            return res;
        }
    }
}

function HTMLVideoElementPlayFunc() {
    console.log("HTMLVideoElementPlayFunc call play");
    try {
        TUtil.setConfigValue("hbbtv_tbrowser", "enable_check_page_transparent", "0");
    } catch (error) {
    }
    return true
}


overrideHTMLElementFunctionHook(HTMLVideoElement.prototype, "play", HTMLVideoElementPlayFunc);