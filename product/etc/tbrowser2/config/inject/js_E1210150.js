console.log("-- inject js_E1210150.js---");
function overrideHTMLElementFunctionHook(target, fun, callbackFunction) {
    let origFunction = target[fun];
    target[fun] = function wrapper() {
        var ret  = callbackFunction.apply(this, arguments); // 调用回调函数 return 1.true:继续执行原始流程 2.false 阻止执行原始流程
        if (ret) {
            var res = origFunction.apply(this, arguments); // 正常执行原始流程，避免出现未知问题
            return res;
        }
    }
}

function HTMLBodyChild() {
    if (arguments[0].id == "video-broadcast-1" ) {
        return false
    }
    return true
}

overrideHTMLElementFunctionHook(HTMLBodyElement.prototype, "removeChild", HTMLBodyChild);