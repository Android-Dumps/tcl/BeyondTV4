console.log("-- inject disable view scroll---");
var node = document.getElementById("appManager");
if(node && node.parentNode)
	node.parentNode.style.position="absolute";

function stopBroadcast() {
    console.log("stopBroadcast()");
    var vidContainer = document.getElementById('tv');

    var html = "";
    html = '<object id="video" type="video/broadcast" style="width:1280px; height: 720px; position: absolute;z-index:1;" class="panel_video"><'+'/object>';

    vidContainer.style.left = '0px';
    vidContainer.style.top = '0px';
    vidContainer.style.width = '1280px';
    vidContainer.style.height = '720px';
    vidContainer.style.zIndex = '1';
    vidContainer.style.position = 'absolute';
    vidContainer.innerHTML = html;

    console.log("PLAYSTATE = " + document.getElementById('video').playState);
    document.getElementById('video').onPlayStateChange = function (state,error){
        console.log("PLAYSTATE = " + state + " ISERROR = " + error);
    };
    setTimeout(function() {
        try{
            console.log("stopBroadcast");
            document.getElementById('video').stop();
        } catch(e) {
            console.log("stopBroadcast failed " + e.message);
        }
    }, 1000);
}