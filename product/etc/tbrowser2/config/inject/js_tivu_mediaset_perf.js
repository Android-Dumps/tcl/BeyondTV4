console.log("-- inject js_tivu_mediaset_perf.js---");
function overrideHTMLElementFunctionHook(target, fun, callbackFunction) {
    let origFunction = target[fun];
    target[fun] = function wrapper() {
        var ret  = callbackFunction.apply(this, arguments); // 调用回调函数 return 1.true:继续执行原始流程 2.false 阻止执行原始流程
        if (ret) {
            var res = origFunction.apply(this, arguments); // 正常执行原始流程，避免出现未知问题
            return res;
        }
    }
}

function HTMLSpanChild() {
    if (arguments[0].innerHTML.indexOf("Selective Attribute DataLayer Library Tag ") > 0 ) {
        return false
    }
    return true
}

overrideHTMLElementFunctionHook(HTMLSpanElement.prototype, "replaceChild", HTMLSpanChild);

document.addEventListener("keyup", function() {
    try {
      var element = document.getElementById('menu');
      if (element != null && element.nodeName == "NAV") {
        var childs = element.childNodes;
        var child1_class = childs[1].attributes.getNamedItem('class').nodeValue;
        if (child1_class != 'section') {
          for (var i = 0; i < childs.length; i++) {
            var child = childs[i];
            var child_class = child.attributes.getNamedItem('class').nodeValue;
            if (i != 1 && child_class != 'section') {
              console.log("tivu---focus error happen,force recover");
              var new_class = document.createAttribute("class");
              new_class.nodeValue = 'section';
              childs[1].attributes.setNamedItem(new_class);
            }
          }
        }
      }
    } catch (error) {
    }
});

if (window.location.href.indexOf("mediaset.net") > -1) {
  window.addEventListener("load", function() {
    try {
      if($ && $.fn && $.fn.animate) {
        let animate_Old = $.fn.animate;
        $.fn.animate = function(a, b, c, d) {
          try {
            let index = this[0].className.indexOf("marginTopBig") // 只涉及这个class
            if (index > -1 &&  b.duration == 200) {
                b.duration = 10
            }
          } catch (error) {}
          return animate_Old.call(this, a,b,c,d)
        }
      }
    } catch (error) {
    }
  }); // load
}
