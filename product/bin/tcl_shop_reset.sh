#!/system/bin/sh

sync
echo "tcl_shop reset start."
if  [ ! -f "/userdata/system_shop_solution_v2" ]&& [ ! -f "/userdata/sita_shop_solution_v2" ]; then
  log -t tcl_shop_reset "no shop_flag in /data."
else
  #settings put secure user_setup_complete 0
  rm -Rf /data/data/com.google.android.tungsten.setupwraith/*
  #rm -Rf /data/system/users/0/*
  rm -Rf /data/system/users/*
  #rm -Rf /data/system/locksettings.db
#  rm -Rf /data/system_de/*
  rm -Rf /data/system_ce/*
  rm -Rf /data/misc/wifi/
  # ifdef VENDOR_EDIT
  # hongsheng2.yu@tcl.com,2022/1/5,don't delete directory
  rm -Rf /data/misc/bluedroid/*
  # endif /* VENDOR_EDIT */
  pm clear com.android.providers.settings
  rm /userdata/system_shop_solution_v2
  #rm /userdata/sita_shop_solution_v2
# reset prop and save persist.sys.locale
  i=1
  while [ $i -le 10 ]; do
  prop_ready=`getprop ro.persistent_properties.ready`
  if [[ "$prop_ready" == "true" ]];then
    log -t tcl_shop_reset  "remove persist prop and reset locale."
    cat /dev/null > /data/property/persistent_properties
    locale=`getprop persist.sys.locale`
    if [ -n "$locale" ];then
      log -t tcl_shop_reset  "locale not null"
      chdomain zygote "setprop persist.sys.locale $locale"
    fi
    break
  else
    log -t tcl_shop_reset  "prop_ready is false."
    sleep 1
  fi
  let i=i+1
  done
# reset prop end
  sync
  log -t tcl_shop_reset  "shop_reset finished"
fi
