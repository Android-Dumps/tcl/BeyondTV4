export GST_PLAYER_PATH=$(pwd)
export PATH=${GST_PLAYER_PATH}/bin:$PATH
export LD_LIBRARY_PATH=/vendor/lib:${GST_PLAYER_PATH}/lib:${GST_PLAYER_PATH}/lib/gstreamer-1.0:$LD_LIBRARY_PATH
export GST_PLUGIN_PATH=${GST_PLAYER_PATH}/lib/gstreamer-1.0/
