#!/vendor/bin/sh

echo "run tcl mw process start"

grep -q Pmode=1 /proc/cmdline
if [ $? -eq 0 ];then
	touch /userdata/sita_P
fi

#export env variable
export LD_LIBRARY_PATH=/vendor/tvos/lib:/vendor/lib:/apex/com.android.vndk.v30/lib
export F1_CONFIG=/tclconfig/json_sys.ini
export F1_OS_SYSTEM=/vendor/bin/sh

#add coredump folder
ulimit -c unlimited
echo "1" > /proc/sys/kernel/core_uses_pid
echo "/dev/null" > /proc/sys/kernel/core_pattern

#logout to usb
/vendor/tvos/bin/log_out -u -i 'fpp' &


#create epg database in RAM
#mkdir -p /userdata/epg 0777 root system
#mount -t tmpfs -o size=64m,mode=0777 tmpfs /userdata/epg
#touch /userdata/epg/EpgData.db
#chmod 0666 /userdata/epg/EpgData.db

#run sitatvservice
/vendor/tvos/bin/sitatvservice all 7 &

/vendor/tvos/bin/tcl_gingad &

#run appmanager
/vendor/tvos/bin/appmanager --app-path=/product/am_apps:/product/am_apps/tplayer --log=logcat --memory-limit-file=/vendor/tvos/bin/am_config.ini &

echo "run tcl mw process end"

/vendor/bin/bt_dongle &

#logcat -G 5M #delete it ,use default value

wait
#while true; do
#        sleep 1
#done
