local zip    = require 'zip'
local _G, assert, ipairs, print, type
    = _G, assert, ipairs, print, type

local str_sub = string.sub
module(...)

-- #######################################################

local operations = {}
local progress = {}

-- #######################################################
local function f_zip_in (evt, id)

    assert(evt.class == 'zip')

    operations[id] = evt
    progress[id] = 0

    -- Just to be able to wait for pcall
    return true
end

-- #######################################################

local function open(evt, id)
    local zip, errnew = zip.open(evt.zip_path, evt.original_path)
    local new_prog = 0
    
    _G.event.post('in', {
        class = 'zip',
        type = 'open',
        -- zip = zip, -- Zip object must be nil until open finish
        id = id,
        progress = new_prog,
        error = errnew
    })
    
    if errnew == nil then
       new_prog = 1
       _G.event.post('in', {
           class = 'zip',
           type = 'open',
           zip = zip,
           id = id,
           progress = new_prog,
           error = errnew
       })
    end
    
    return new_prog, errnew
end

-- #######################################################

local function extract (evt, id)
    local entry = evt.entry_path;

--    if entry and str_sub(entry,1,1) == '/' then
--       entry = str_sub(entry, 2)
--    end

    local force = true;
    if evt.force ~= nil then
        force = evt.force
    end

    local progress,errmsg = zip.extract(evt.zip, evt.dest_dir, entry, force, id)

    -- Don't send event if it's the first call, only if there's an error
    -- if errmsg ~= nil or progress > 0 then
        _G.event.post('in', {
            class = 'zip',
            type = 'extract',
            zip = evt.zip,
            id = id,
            dest_dir = evt.original_path,
            entry_path = evt.entry_path,
            progress = progress,
            error = errmsg
        })
    -- end

    return progress, errmsg 
end

-- #######################################################

local function save(evt, id)

    local force = true;
    if evt.force ~= nil then
        force = evt.force
    end

    local progress,errmsg = zip.save(evt.zip, evt.zip_path, force, id)

    -- Don't send event if it's the first call, only if there's an error
    -- if errmsg ~= nil or progress > 0 then
        _G.event.post('in', {
            class = 'zip',
            type = 'save',
            zip = evt.zip,
            zip_path = evt.original_path,
            id = id,
            progress = progress,
            error = errmsg
        })
    -- end

    return progress, errmsg
end

-- #######################################################

local function cancel(evt, id) 
    local errmsg, progress
    
    errmsg = zip.cancel(evt.id)
    _G.event.post('in', {
        class = 'zip',
        type = 'cancel',
        id = evt.id,
        error = errmsg
    })
    progress = 1
    
    return progress, errmsg
end

-- #######################################################

local function f_zip_out (id)

    local evt = operations[id]
    local errmsg = nil;

    if evt.type == 'open' then
        progress[id],errmsg = open(evt, id)
    elseif evt.type == 'extract' then
        progress[id],errmsg = extract(evt, id)
    elseif evt.type == 'save' then
        progress[id],errmsg = save(evt, id)
    elseif evt.type == 'cancel' then
        progress[id],errmsg = cancel(evt, id)
    else
        errmsg = "error"
        _G.event.post('in', {
            class = 'zip',
            error = errmsg,
        })
    end

    return progress[id], errmsg
end

-- #######################################################

return { f_zip_in, f_zip_out }
