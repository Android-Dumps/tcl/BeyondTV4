local buffer = require 'buffer'
local socket = require 'socket'
local _G, assert, ipairs, print
    = _G, assert, ipairs, print

local str_len = string.len
local t_remove = table.remove

module(...)

local associated_devs= {}
local nextDevice = 0

local function f_unbind(association) 
    for i, dev in ipairs (associated_devs) do
        if (dev.device == association) then
            dev.device:close()
            t_remove(associated_devs, i)
        end
    end
end

local function f_get(association)
    for i, dev in ipairs (associated_devs) do
        if (dev.device == association) then
            return dev
        end
    end
    return nil
end

local function f_event (evt)

    assert(evt.class == 'udp')

    if evt.type == 'bind' then

        local laddr = '*'
        local lport = evt.local_port or 0
        local status, err
        local isClient

        local udp = socket.udp()

        if evt.broadcast then
            udp:setoption('broadcast', true)
            evt.host = '255.255.255.255'
        end

        if evt.host ~= nil then

            local host_port = evt.port or 0
            status, err = udp:setpeername(evt.host, host_port)
            if err == "host not found" then
                err = "unknown host"
            end
            isClient = true

        else 

            if evt.interface_addr ~= nil then
                if evt.interface_addr:lower() == 'localhost' then
                    laddr = '*'
                elseif evt.interface_addr:lower() == '127.0.0.1' then
                    laddr = '*'
                else
                    laddr = evt.interface_addr
                end
            end

            status, err = udp:setsockname(laddr, lport)
            if err == "host not found" then
                err = "invalid address"
            end
            isClient = false

        end

        -- Convert the errors to fit NCLua needs.
        if err ~= nil then
            if err == "address already in use" then
                err = "port unavailable"
            end
        end

        local dtype = 'string'
        if evt.data_type == 'buffer' then
            dtype = evt.data_type
        end

        local max_dlength = evt.max_data_length or nil

        associated_devs[#associated_devs+1] = {
            device = udp,
            data_type = dtype,
            max_data_length = max_dlength,
            error_msg = err,
            client_mode = isClient
        }

        if evt.interface_addr == nil then
            _G.event.post('in', {
                class = 'udp',
                type  = 'bind',
                local_port = lport,
                association = udp,
                error = err,
            })
        else
            _G.event.post('in', {
                class = 'udp',
                type  = 'bind',
                interface_addr = evt.interface_addr,
                local_port = lport,
                association = udp,
                error = err,
            })
        end

    elseif evt.type == 'data' then

        local udp = evt.association

        if udp == nil then

            _G.event.post('in', {
                class = 'udp',
                type  = 'data',
                association = evt.association,
                error = "invalid association",
            })

        else

            local device = f_get(udp)

            if device == nil then

                _G.event.post('in', {
                    class = 'udp',
                    type  = 'data',
                    association = evt.association,
                    error = "invalid association",
                })

            elseif device.error_msg ~= nil then

                _G.event.post('in', {
                    class = 'udp',
                    type  = 'data',
                    association = evt.association,
                    error = device.error_msg,
                })

            else

                local str_send = evt.value or evt.data:toString()
                if str_send ~= nil then
                    if device.client_mode then
                        udp:send(str_send)
                    else
                        _G.event.post('in', {
                            class = 'udp',
                            type  = 'data',
                            association = evt.association,
                            error = "invalid association",
                        })
                    end

                end

            end
        end

    elseif evt.type == 'unbind' then
        f_unbind(evt.association)
    else
        _G.event.post('in', {
            class = 'udp', 
            error = "Connection error",
        })
    end

end

-- Receive function for associated devices
local function f_udp_receive () 

    if #associated_devs > 0 then
        nextDevice = (nextDevice + 1)%(#associated_devs)

        local adevice = associated_devs[nextDevice+1]
        local dev = adevice.device

        if dev ~= nil then

            local line, host, port
            dev:settimeout(0.5)

            line, err = dev:receive(adevice.max_data_length)

            if line then
                if adevice.data_type == 'buffer' then
                    local lbuffer = buffer.new(line)
                    _G.event.post('in', {
                        class       = 'udp',
                        type        = 'data',
                        association = dev,
                        data        = lbuffer,
                    })

                else
                    _G.event.post('in', {
                        class       = 'udp',
                        type        = 'data',
                        association = dev,
                        value       = line,
                    })
                end
            end
        end
    else
        nextDevice = 0
    end

    return #associated_devs
end

return { f_event, f_udp_receive }
