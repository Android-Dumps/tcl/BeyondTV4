local eitvutil = require 'eitvutil'
local buffer = require 'buffer'
local socket = require 'socket'
local _G, assert, ipairs, print
    = _G, assert, ipairs, print

local str_len = string.len
local t_remove = table.remove


module(...)

--Client mode variables
-- local T = {}
local clients = {}
local clients_to_remove = {}
local nextClient = 0
local closed_by_user = false

--Server mode variables
local servers = {}
local server_clients = {}
local server_clients_to_remove = {}
local max_n_clients = 10
local nextServer = 0
local nextServerClient = 0

local data_type = {}

local tcp_userdata = 'tcp{client}'

local function f_close_disconnected_clients (clients_idx, client_list)
    -- Remove clients that lost connection
    while #clients_idx > 0 do
        local idx = clients_idx[#clients_idx]
        if client_list[idx] ~= nil then
            client_list[idx]:close()
        end
        t_remove(client_list, idx)
        clients_idx[#clients_idx] = nil -- pop
    end
end

local function f_disconnect (client)

     for i, cli in ipairs (clients) do
        if (cli == client) then
            cli:close()
            t_remove(clients, i)
        end
    end

end

local function f_unlisten(port) 

    for i, serv in ipairs(servers) do
        local _, lport = serv:getsockname()
        -- For some reason, direct comparison is failing...
        if (port - lport) == 0 then
            serv:close()
            t_remove(servers,i)
        end
    end

end

local function f_disconnect_server_clients ()
     for i, cli in ipairs (server_clients) do
        cli:close()
        t_remove(server_clients, i)
    end

end

local function f_get_err_message(laddr, lport, err)

    if err == "address already in use" then
        return "port unavailable"
    end

    if err == "host not found" then
        return "invalid address"
    end

    return err
end

local function f_event (evt)

    assert(evt.class == 'tcp')

    local laddr = evt.interface_addr or '*'
    local lport = evt.local_port or 0

    local interface = '*'
    if evt.interface_addr ~= nil then
        if evt.interface_addr:lower() == 'localhost' then
            interface = '*'
        elseif evt.interface_addr == '127.0.0.1' then
            interface = '*'
        else
            interface = evt.interface_addr
        end
    end

    if evt.type == 'connect' then
        local client = socket.tcp()
        closed_by_user = false

        client:setoption('reuseaddr', true)

        if evt.timeout ~= nil then
            client:settimeout(evt.timeout)
        end

        local res, err = client:connect(evt.host, evt.port)
        laddr, lport = client:getsockname()

        if res == nil then

            --Get right error message in case of connection fail due
            --to another connection in the same interface_addr/local_port
            --ABNT NBR 15606-2:2018, pg. 153
            err = f_get_err_message(laddr, lport, err)

            _G.event.post('in', {
                    class = 'tcp',
                    type  = 'connect',
                    host  = evt.host,
                    port  = evt.port,
                    interface_addr = laddr,
                    local_port = lport,
                    error = err,
                })
        else
            clients[#clients+1] = client

            if evt.data_type ~= 'buffer' then
                evt.data_type = 'string'
            end

            data_type[client] = {
                data_type = evt.data_type
            }

            _G.event.post('in', {
                    class = 'tcp',
                    type  = 'connect',
                    host  = evt.host,
                    port  = evt.port,
                    interface_addr = laddr,
                    local_port = lport,
                    connection = client,
                })
        end

    elseif evt.type == 'listen' then

        local server, err = socket.bind(interface, evt.local_port, max_connections)
        if server == nil then

            --Get right error message in case of connection fail due
            --to another connection in the same interface_addr/local_port
            --ABNT NBR 15606-2:2018, pg. 153
            err = f_get_err_message(interface, evt.local_port, err)

            _G.event.post('in', {
                class = 'tcp',
                type  = 'listen',
                host = interface,
                port = evt.local_port,
                interface_addr = interface,
                local_port = evt.local_port,
                error = err,
            })           
        else 
            servers[#servers+1] = server

            if evt.data_type ~= 'buffer' then
                evt.data_type = 'string'
            end

            data_type[server] = {
                data_type = evt.data_type
            }
        end
        

    elseif evt.type == 'data' then
        local str_send, err
        

        -- Evaluate close requests, if any, to avoid sending
        -- data to a client already closed
        if #clients_to_remove > 0 then
            f_close_disconnected_clients(clients_to_remove, clients)
        end
        if #server_clients_to_remove > 0 then
            f_close_disconnected_clients(server_clients_to_remove, server_clients)
        end

       if not eitvutil.checkUserData(evt.connection, tcp_userdata) then
            _G.event.post('in', {
                class      = 'tcp',
                type       = 'data',
                connection = evt.connection,
                error = "invalid connection",
            })
       end

        if evt.timeout ~= nil then
            evt.connection:settimeout(evt.timeout)
        end

        if data_type[evt.connection].data_type == 'buffer' then
            str_send = evt.data:toString()
            _, err = evt.connection:send(str_send..'\n')
        else
            str_send = evt.value
            _, err = evt.connection:send(evt.value..'\n')
        end

        if err then
            _G.event.post('in', {
                class      = 'tcp',
                type       = 'data',
                connection = evt.connection,
                error = "invalid connection",
            })
        end

    elseif evt.type == 'disconnect' then
        closed_by_user = true
        if eitvutil.checkUserData(evt.connection, tcp_userdata) then
            f_disconnect(evt.connection)
        end

    elseif evt.type == 'unlisten' then
        f_unlisten(evt.local_port)
        f_disconnect_server_clients()

    else
        _G.event.post('in', {
            class = 'tcp', 
            error = "unexpected error",
        })
    end

end

local function f_client () 

    -- Remove clients that lost connection
    f_close_disconnected_clients(clients_to_remove, clients)

    if #clients > 0 then
        nextClient = (nextClient + 1)%(#clients)

        local client = clients[nextClient+1]

        client:settimeout(0.3)
        local line, err = client:receive()

        if err == "closed" and not closed_by_user then
            _G.event.post('in', {
                class       = 'tcp',
                type        = 'disconnect',
                connection  = client,
                error       = err,
            })

            -- Save index to remove client in the next iteration
            -- so that post succeeds
            clients_to_remove[#clients_to_remove+1] = nextClient+1
        end

        if not err then
            if data_type[client].data_type == 'buffer' then
                local lbuffer = buffer.new(line)
                _G.event.post('in', {
                    class      = 'tcp',
                    type       = 'data',
                    data       = lbuffer,
                    connection = client,
                })
            else
                _G.event.post('in', {
                    class      = 'tcp',
                    type       = 'data',
                    value      = line,
                    connection = client,
                })
            end
        end
    else
        nextClient = 0
    end


    return #clients    
end

local function f_server ()

    if #servers > 0 then
        nextServer = (nextServer + 1)%(#servers)
        local server = servers[nextServer + 1] --get last
        server:settimeout(0.5)

        local client, err = server:accept()

        if client ~= nil then
            server_clients[#server_clients+1] = client

            data_type[client] = data_type[server]

            local host, port = client:getpeername()
            -- local lhost, lport = server:getsockname()
            local lhost, lport = client:getsockname()

            _G.event.post('in', {
                    class = 'tcp',
                    type  = 'connect',
                    host  = host,
                    port  = port,
                    interface_addr = lhost,
                    local_port = lport,
                    connection = client,
                })

        end
    else
        nextServer = 0
    end

    return { #server_clients, #servers }
end

local function f_client_handler ()

    -- Remove clients that lost connection
    f_close_disconnected_clients(server_clients_to_remove, server_clients)

    if #server_clients > 0 then
        nextServerClient = (nextServerClient + 1)%(#server_clients)

        local client = server_clients[nextServerClient+1]

        client:settimeout(0.5)
        local line, err = client:receive()

        if err == "closed" then
            _G.event.post('in', {
                class       = 'tcp',
                type        = 'disconnect',
                connection  = client,
                error       = err,
            })

            server_clients_to_remove[#server_clients_to_remove+1] = nextServerClient+1
        end

        if err == nil then
            if data_type[client] ~= nil then

                if data_type[client].data_type == 'buffer' then
                    local lbuffer = buffer.new(line)
                    _G.event.post('in', {
                        class      = 'tcp',
                        type       = 'data',
                        data       = lbuffer,
                        connection = client,
                    })
                else
                    _G.event.post('in', {
                        class      = 'tcp',
                        type       = 'data',
                        value      = line,
                        connection = client,
                    })
                end

            else
                _G.event.post('in', {
                    class      = 'tcp',
                    type       = 'data',
                    value      = line,
                    connection = client,
                })
            end
        end
    else
        nextServerClient = 0
    end

    return #server_clients
end

return { f_event, f_client, f_server, f_client_handler }
