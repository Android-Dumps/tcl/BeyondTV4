---------------------------------------------------------------------
-- SOAP over HTTP.
-- See Copyright notice in license.html
-- $Id: http.lua,v 1.1 2004/03/24 19:27:37 tomas Exp $
---------------------------------------------------------------------

require"socket"
require"socket.http"
require"soap"

local soapsocket = socket.http

soap.http = {}

---------------------------------------------------------------------
-- Call a remote method.
-- @param url String with the location of the server.
-- @param namespace
---------------------------------------------------------------------
function soap.http.call (url, namespace, method, entries, headers)
    local t = {}
    soapsink = ltn12.sink.table(t)
	body = soap.encode(namespace, method, entries, headers)
	local body, code, headers, status = soapsocket.request {
		method = "POST",
		url = url,
		headers = {
			["Content-Type"] = "text/xml; charset=utf-8",
			["Content-Length"] = body:len(),
			["SOAPAction"] = '"'..method..'"',
		},
		source = ltn12.source.string(body),
		sink = soapsink
	}

	if tonumber (code) == 200 then
		return soap.decode (t[1])
	else
		return nil
	end
end
