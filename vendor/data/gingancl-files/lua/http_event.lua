local http = require 'socket.http'
local https = require "ssl.https"
local buffer = require 'buffer'
local ltn12 = require 'ltn12'
local _G, assert, ipairs, print, type
    = _G, assert, ipairs, print, type

module(...)

local T = {}
local requests = {}
local USER_AGENT_VALUE = "LuaSocket"

local function f_http_in (evt, id)

    assert(evt.class == 'http')

    if evt.type == 'request' then
        local request = {}

        request.response_data_type = evt.response_data_type
        request.method  = evt.method
        request.cert    = evt.cert
        request.uri     = evt.uri
        request.session = evt.session
        request.headers = evt.headers
        request.body    = evt.body
        request.timeout = evt.timeout
        requests[id]    = request

        if evt.session ~= nil then
            T[evt.session] = {}
            T[evt.session]["cancel"] = false
        end

    elseif evt.type == 'cancel' then
        if evt.session ~= nil and T[evt.session] then
            T[evt.session]["cancel"] = true
        end
    else
        _G.event.post('in', {
            class = 'http',
            error = "error",
        })
    end

    -- Just to be able to wait for pcall
    return true
end

local function concatTable (table)
    local concated = ""
    for i, tmp in ipairs (table) do
        concated = concated..tmp
    end
    return concated
end

local function bodyToString (body)
    local strRet = ""
    if type(body) == 'userdata' then
        strRet = body:toString()
    else
        strRet = body
    end
    return strRet
end

local function f_http_out (id)
    local evt = requests[id]

    if evt ~= nil then
        local response = {}
        local resp = {}

        resp.class   = "http"
        resp.type    = "response"
        resp.method  = evt.method
        resp.uri     = evt.uri
        resp.session = evt.session

        local reqt = {
            url = evt.uri,
            sink = ltn12.sink.table(response),
            method = evt.method
        }
        if evt.headers ~= nil then
            reqt.headers = evt.headers
        end
        if evt.body ~= nil then
            local strBody = bodyToString(evt.body)
            reqt.source = ltn12.source.string(strBody)
        end

        http.TIMEOUT = evt.timeout
        http.USERAGENT = USER_AGENT_VALUE

        r, c, h = http.request(reqt)

        if r == nil then
            --Try with HTTPS if fail
            if evt.cert ~= nil then
                reqt.certificate = evt.cert
            end
            r, c, h = https.request(reqt)
        end

        local strResponse = concatTable(response)

        if not (T[evt.session] ~= nil and T[evt.session]["cancel"] == true) then
            if r == nil then
                resp.error = c
            else
                if evt.response_data_type == 'buffer' then
                    resp.body = buffer.new(strResponse)
                else
                    resp.body = strResponse
                end

                resp.headers  = h
                resp.code     = c
                resp.finished = true
            end
            _G.event.post('in', resp)
        end
    end
end


return { f_http_in, f_http_out }
