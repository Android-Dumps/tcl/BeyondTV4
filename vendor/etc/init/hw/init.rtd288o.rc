#
# Copyright 2014 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import /vendor/etc/init/hw/init.${ro.hardware}.usb.rc
import /vendor/etc/init/hw/init.${ro.hardware}.diag.rc
# ifdef VENDOR_EDIT
# pengzhao_chen@tcl.com, 2020/10/22, [CNROM-5] decoupling tcl device
import /vendor/etc/init/hw/tclinit.rc
# endif /* VENDOR_EDIT */

on early-init
    mount debugfs debugfs /sys/kernel/debug mode=0755


on init
    # Android creates by-name disk links with the disk controller
    # in the generated path, so that the names pulled out of the GPT
    # can be associated with the correct disk. Create a shortcut to
    # /dev/block/by-name so that we can use the same fstabs everywhere.
    symlink /dev/block/pci/pci0000:00/0000:00:01.0/by-name /dev/block/by-name

    symlink /sdcard /storage/sdcard0

    # ZRAM options
    write /sys/block/zram0/comp_algorithm zacc
    write /sys/block/zram0/max_comp_streams 2

    # KSM options
    #write /sys/kernel/mm/ksm/pages_to_scan 100
    #write /sys/kernel/mm/ksm/sleep_millisecs 500
    #write /sys/kernel/mm/ksm/run 1

    #  Mount the sep filesystem
    mount sepfs sepfs /sys/fs/sepfs

    chmod 0666 /sys/realtek_boards/reclaim_dvr
    chmod 0000 /sys/bus/platform/devices/18010800.emmc/em_open_log
    chmod 0666 /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
    chmod 0666 /sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq
    chmod 0666 /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq
    chmod 0666 /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed
    chmod 0666 /sys/class/rtk-pwm/pwm-I-1/duty
    chmod 0666 /sys/class/rtk-pwm/pwm-I-1/apply
    chown system system /sys/power/pm_userresume
    chmod 0660 /sys/power/pm_userresume
    chmod 0660 /sys/power/state
    chmod 0440 /proc/cmdline

    chmod 0666 /sys/devices/virtual/misc/emcu_kdv/emcu_clr_pm_task
    chmod 0666 /sys/devices/virtual/misc/emcu_kdv/emcu_clr_share_mem
    chmod 0666 /sys/devices/virtual/misc/emcu_kdv/emcu_get_rec_time
    chmod 0666 /sys/devices/virtual/misc/emcu_kdv/emcu_get_sleep_dur
    chmod 0666 /sys/devices/virtual/misc/emcu_kdv/emcu_get_wakeup_source
    chmod 0666 /sys/devices/virtual/misc/emcu_kdv/emcu_get_wakeup_status
    chmod 0666 /sys/devices/virtual/misc/emcu_kdv/emcu_set_RTC
    chmod 0666 /sys/devices/virtual/misc/emcu_kdv/emcu_set_STM
    chmod 0666 /sys/devices/virtual/misc/emcu_kdv/emcu_set_WOL
    chmod 0666 /sys/devices/virtual/misc/emcu_kdv/emcu_set_WOWLAN
    chmod 0666 /sys/devices/virtual/misc/emcu_kdv/emcu_set_cur_time
    chmod 0666 /sys/devices/virtual/misc/emcu_kdv/emcu_set_sharemem
    chmod 0666 /sys/devices/virtual/misc/emcu_kdv/emcu_set_sharemem_mask
    chmod 0666 /sys/devices/virtual/misc/emcu_kdv/emcu_setup_cec
    chmod 0666 /sys/devices/virtual/misc/emcu_kdv/emcu_setup_irda
    chmod 0666 /sys/devices/virtual/misc/emcu_kdv/emcu_setup_irda_in_code
    chmod 0666 /sys/devices/virtual/misc/emcu_kdv/emcu_setup_keypad_multi
    chmod 0666 /sys/devices/virtual/misc/emcu_kdv/emcu_setup_mhl
    chmod 0666 /sys/devices/virtual/misc/emcu_kdv/emcu_setup_power_en
    chmod 0666 /sys/devices/virtual/misc/emcu_kdv/emcu_setup_power_en_in_name
    chmod 0666 /sys/devices/virtual/misc/emcu_kdv/emcu_setup_ppsource
    chmod 0666 /sys/devices/virtual/misc/emcu_kdv/emcu_setup_uart
    chmod 0666 /sys/devices/virtual/misc/emcu_kdv/emcu_setup_wakeup_min
    chmod 0666 /sys/devices/virtual/misc/emcu_kdv/emcu_setup_wakeup_sec
    chmod 0666 /sys/devices/virtual/misc/emcu_kdv/emcu_setup_wakeup_timer
    chmod 0666 /sys/devices/virtual/misc/emcu_kdv/emcu_setup_wakeup_timer1
    chmod 0666 /sys/devices/virtual/misc/emcu_kdv/emcu_show_help
    chmod 0666 /sys/devices/virtual/misc/emcu_kdv/emcu_show_share_mem
    chmod 0666 /sys/devices/virtual/misc/emcu_kdv/emcu_show_version
    chmod 0666 /sys/venus_ir/ir_gen_keycode
    chmod 0666 /sys/kernel/debug/sync/sw_sync

    chown system media /sys/devices/virtual/misc/emcu_kdv/emcu_clr_pm_task
    chown system media /sys/devices/virtual/misc/emcu_kdv/emcu_clr_share_mem
    chown system media /sys/devices/virtual/misc/emcu_kdv/emcu_get_rec_time
    chown system media /sys/devices/virtual/misc/emcu_kdv/emcu_get_sleep_dur
    chown system media /sys/devices/virtual/misc/emcu_kdv/emcu_get_wakeup_source
    chown system media /sys/devices/virtual/misc/emcu_kdv/emcu_get_wakeup_status
    chown system media /sys/devices/virtual/misc/emcu_kdv/emcu_set_RTC
    chown system media /sys/devices/virtual/misc/emcu_kdv/emcu_set_STM
    chown system media /sys/devices/virtual/misc/emcu_kdv/emcu_set_WOL
    chown system media /sys/devices/virtual/misc/emcu_kdv/emcu_set_WOWLAN
    chown system media /sys/devices/virtual/misc/emcu_kdv/emcu_set_cur_time
    chown system media /sys/devices/virtual/misc/emcu_kdv/emcu_set_sharemem
    chown system media /sys/devices/virtual/misc/emcu_kdv/emcu_set_sharemem_mask
    chown system media /sys/devices/virtual/misc/emcu_kdv/emcu_setup_cec
    chown system media /sys/devices/virtual/misc/emcu_kdv/emcu_setup_irda
    chown system media /sys/devices/virtual/misc/emcu_kdv/emcu_setup_irda_in_code
    chown system media /sys/devices/virtual/misc/emcu_kdv/emcu_setup_keypad_multi
    chown system media /sys/devices/virtual/misc/emcu_kdv/emcu_setup_mhl
    chown system media /sys/devices/virtual/misc/emcu_kdv/emcu_setup_power_en
    chown system media /sys/devices/virtual/misc/emcu_kdv/emcu_setup_power_en_in_name
    chown system media /sys/devices/virtual/misc/emcu_kdv/emcu_setup_ppsource
    chown system media /sys/devices/virtual/misc/emcu_kdv/emcu_setup_uart
    chown system media /sys/devices/virtual/misc/emcu_kdv/emcu_setup_wakeup_min
    chown system media /sys/devices/virtual/misc/emcu_kdv/emcu_setup_wakeup_sec
    chown system media /sys/devices/virtual/misc/emcu_kdv/emcu_setup_wakeup_timer
    chown system media /sys/devices/virtual/misc/emcu_kdv/emcu_setup_wakeup_timer1
    chown system media /sys/devices/virtual/misc/emcu_kdv/emcu_show_help
    chown system media /sys/devices/virtual/misc/emcu_kdv/emcu_show_share_mem
    chown system media /sys/devices/virtual/misc/emcu_kdv/emcu_show_version
    chown system drmrpc /sys/venus_ir/ir_gen_keycode

    # Create UDS structure for base VR services.
    mkdir /dev/socket/pdx 0775 system system
    mkdir /dev/socket/pdx/system 0775 system system
    mkdir /dev/socket/pdx/system/buffer_hub 0775 system system
    mkdir /dev/socket/pdx/system/performance 0775 system system
    mkdir /dev/socket/pdx/system/vr 0775 system system
    mkdir /dev/socket/pdx/system/vr/display 0775 system system
    mkdir /dev/socket/pdx/system/vr/pose 0775 system system
    mkdir /dev/socket/pdx/system/vr/sensors 0775 system system

    chown system system /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
    chown system system /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed

    start console

on fs
   mkdir /logs 0770 system log
    mkdir /factory 0775 system system

    mount_all /vendor/etc/fstab.${ro.hardware}
    restorecon_recursive /factory
    chmod 0666 /dev/rtd-logger 

on late-fs
    write /sys/block/dm-0/queue/read_ahead_kb 2048

on boot
    chown root system /sys/power/wake_lock
    chown root system /sys/power/wake_unlock

    # Assign TCP buffer thresholds to be ceiling value of technology maximums
    # Increased technology maximums should be reflected here.
    write /proc/sys/net/core/rmem_max  1500000
    write /proc/sys/net/core/wmem_max  1500000
    write /proc/sys/net/ipv4/tcp_limit_output_bytes 1500000
    setprop net.tcp.buffersize.wifi 500000,1000000,1500000,500000,1000000,1500000

    chown system system /sys/class/backlight/psb-bl/brightness
    chown system system /sys/class/backlight/psb-bl/max_brightness
    chown system system /sys/class/backlight/psb-bl/actual_brightness
    chmod 0660 /sys/class/backlight/psb-bl/brightness
    chmod 0660 /sys/class/backlight/psb-bl/max_brightness
    chmod 0660 /sys/class/backlight/psb-bl/actual_brightness

    write /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor performance

    # Wifi
    setprop wifi.interface wlan0
    # Set correct country code
    exec - root root system -- /system/bin/init.rtd288o.countrycode.sh

    # Wifi firmware reload path
    chown wifi wifi /sys/module/bcmdhd/parameters/firmware_path

    # bluetooth
    # change back to bluetooth from system
    chown bluetooth net_bt_admin /data/misc/bluetooth
    mkdir /data/misc/bluedroid 0770 bluetooth net_bt_admin
    mkdir /data/vendor/bluetooth 0770 bluetooth net_bt_admin
    # bluetooth LPM
    chown bluetooth net_bt_admin /proc/bluetooth/sleep/lpm
    chown bluetooth net_bt_admin /proc/bluetooth/sleep/btwrite

    # orient
    setprop config.override_forced_orient true

    #enable USB
    write /proc/usercalls 1

    #USB device
    chmod 0660 /dev/rtkbt_dev
    chown bluetooth net_bt_admin /dev/rtkbt_dev

    #fusion device
    #insmod /lib/modules/4.4.3+/kernel/drivers/char/fusion.ko
    exec u:r:vendor_modprobe:s0 -- /vendor/bin/insmod /vendor/lib/modules/fusion.ko

    #TCL device
    exec u:r:vendor_modprobe:s0 -- /vendor/bin/insmod /vendor/lib/modules/tcl_sim_kb.ko
    exec u:r:vendor_modprobe:s0 -- /vendor/bin/insmod /vendor/lib/modules/tcl_sim_tp.ko

    # rfkill
    chmod 0660 /sys/class/rfkill/rfkill0/state
    chmod 0660 /sys/class/rfkill/rfkill0/type
    chown bluetooth net_bt_admin /sys/class/rfkill/rfkill0/state
    chown bluetooth net_bt_admin /sys/class/rfkill/rfkill0/type

    # bluetooth MAC address programming
    chown bluetooth net_bt_admin ro.bt.bdaddr_path
    chown bluetooth net_bt_admin /system/etc/bluetooth
    chown bluetooth net_bt_admin /data/vendor/bluetooth

    service dhcpcd_bnep0 /system/bin/dhcpcd -BKLG
    disabled
    oneshot
    service dhcpcd_bnep1 /system/bin/dhcpcd -BKLG
    disabled
    oneshot
    service dhcpcd_bnep2 /system/bin/dhcpcd -BKLG
    disabled
    oneshot
    service dhcpcd_bnep3 /system/bin/dhcpcd -BKLG
    disabled
    oneshot
    service dhcpcd_bnep4 /system/bin/dhcpcd -BKLG
    disabled
    oneshot
    service dhcpcd_bt-pan /system/bin/dhcpcd -ABKL
    class main
    disabled
    oneshot
    service iprenew_bt-pan /system/bin/dhcpcd -n
    class main
    disabled
    oneshot

on shutdown
    # emcu share memory
    write /sys/devices/virtual/misc/emcu_kdv/emcu_setup_power_en_in_name 2,PIN_POWER_EN,PIN_POWER_EN_1
    #write /sys/devices/virtual/misc/emcu_kdv/emcu_setup_irda_in_code 1,0x74
    #write /sys/devices/virtual/misc/emcu_kdv/emcu_setup_irda 0x8,0xc,0xf2a0d5,0xfef010,0xf300cf,0xf310ce,0xf320cd,0xf330cc,0xf340cb,0xf350ca,0xf360c9,0xf370c8,0xf380c7,0xf390c6
	write /sys/devices/virtual/misc/emcu_kdv/emcu_setup_keypad_multi 1,0x74000a00000001

on post-fs
    # set RLIMIT_MEMLOCK to 64MB
    setrlimit 8 67108864 67108864

    #adjust some parameter -rtk
    write /sys/module/kernel/parameters/alignment 3

    # Performance tweaks for interactive governor
    chown system system /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq
    chown system system /sys/devices/system/cpu/cpufreq/interactive/timer_rate
    chown system system /sys/devices/system/cpu/cpufreq/interactive/go_hispeed_load
    chown system system /sys/devices/system/cpu/cpufreq/interactive/boostpulse
    chown system system /sys/devices/system/cpu/cpufreq/interactive/touchboostpulse
    chown system system /sys/devices/system/cpu/cpufreq/interactive/touchboostpulse_duration
    chown system system /sys/devices/system/cpu/cpufreq/interactive/touchboost_freq
    chmod 0220 /sys/devices/system/cpu/cpufreq/interactive/touchboostpulse
    chmod 0220 /sys/devices/system/cpu/cpufreq/interactive/boostpulse

    # Tune interactive governor parameters for Android TV UI animations
    write /sys/devices/system/cpu/cpufreq/interactive/timer_rate 4000
    write /sys/devices/system/cpu/cpufreq/interactive/timer_slack 16000
    write /sys/devices/system/cpu/cpufreq/interactive/go_hispeed_load 90
    write /sys/devices/system/cpu/cpufreq/interactive/target_loads 70
    write /sys/devices/system/cpu/cpufreq/interactive/boostpulse_duration 800000

    # SEP
    copy /system/etc/security/sep_policy.conf /sys/fs/sepfs/load
    chown system system /dev/dx_sep_q0
    chmod 0666 /dev/dx_sep_q0

    # hdmi cec
    chown system system /sys/module/drm_intel_mid/parameters/hdmi_state
    chmod 0440 /sys/module/drm_intel_mid/parameters/hdmi_state
    chown system system /sys/module/drm_intel_mid/parameters/hdmi_edid_src_phy_addr
    chmod 0440 /sys/module/drm_intel_mid/parameters/hdmi_edid_src_phy_addr

    # ifdef VENDOR_EDIT
    # weizhao.ouyang@tcl.com, 2021/04/23, add for tcl_healthinfo permission
    chown root system /proc/tcl_healthinfo
    chown root system /proc/tcl_healthinfo/alloc_fail_order
    chown root system /proc/tcl_healthinfo/alloc_wait
    chown root system /proc/tcl_healthinfo/cpu_rt_info
    chown root system /proc/tcl_healthinfo/dstate
    chown root system /proc/tcl_healthinfo/emcdrv_iowait
    chown root system /proc/tcl_healthinfo/enable
    chown root system /proc/tcl_healthinfo/fsync_wait
    chown root system /proc/tcl_healthinfo/ion_wait
    chown root system /proc/tcl_healthinfo/iowait
    chown root system /proc/tcl_healthinfo/sched_latency
    chown root system /proc/tcl_healthinfo/cpu_dstate
    chown root system /proc/tcl_healthinfo/cpu_dstate/cpu_dstate_h_ms
    chown root system /proc/tcl_healthinfo/cpu_dstate/cpu_dstate_l_ms
    chown root system /proc/tcl_healthinfo/cpu_dstate/cpu_dstate_log_ms
    chown root system /proc/tcl_healthinfo/cpu_dstate/ctrl
    chown root system /proc/tcl_healthinfo/cpu_dstate/logp

    chown root system /proc/tcl_healthinfo/cpu_rtsched
    chown root system /proc/tcl_healthinfo/cpu_rtsched/cpu_rt_sched_latency_h_ns
    chown root system /proc/tcl_healthinfo/cpu_rtsched/cpu_rt_sched_latency_l_ns
    chown root system /proc/tcl_healthinfo/cpu_rtsched/ctrl
    chown root system /proc/tcl_healthinfo/cpu_rtsched/logp

    chown root system /proc/tcl_healthinfo/cpu_sched
    chown root system /proc/tcl_healthinfo/cpu_sched/cpu_sched_latency_h_ms
    chown root system /proc/tcl_healthinfo/cpu_sched/cpu_sched_latency_l_ms
    chown root system /proc/tcl_healthinfo/cpu_sched/cpu_sched_latency_log_ms
    chown root system /proc/tcl_healthinfo/cpu_sched/ctrl
    chown root system /proc/tcl_healthinfo/cpu_sched/logp

    chown root system /proc/tcl_healthinfo/fsync
    chown root system /proc/tcl_healthinfo/fsync/fsync_h_ms
    chown root system /proc/tcl_healthinfo/fsync/fsync_l_ms
    chown root system /proc/tcl_healthinfo/fsync/fsync_log_ms
    chown root system /proc/tcl_healthinfo/fsync/ctrl
    chown root system /proc/tcl_healthinfo/fsync/logp

    chown root system /proc/tcl_healthinfo/io_wait
    chown root system /proc/tcl_healthinfo/io_wait/iowait_h_ms
    chown root system /proc/tcl_healthinfo/io_wait/iowait_l_ms
    chown root system /proc/tcl_healthinfo/io_wait/iowait_log_ms
    chown root system /proc/tcl_healthinfo/io_wait/ctrl
    chown root system /proc/tcl_healthinfo/io_wait/logp

    chown root system /proc/tcl_healthinfo/iolatency
    chown root system /proc/tcl_healthinfo/iolatency/iolat_blk_h_ms
    chown root system /proc/tcl_healthinfo/iolatency/iolat_blk_l_ms
    chown root system /proc/tcl_healthinfo/iolatency/iolat_blk_log_ms
    chown root system /proc/tcl_healthinfo/iolatency/iolat_flash_h_ms
    chown root system /proc/tcl_healthinfo/iolatency/iolat_flash_l_ms
    chown root system /proc/tcl_healthinfo/iolatency/iolat_flash_log_ms
    chown root system /proc/tcl_healthinfo/iolatency/ctrl
    chown root system /proc/tcl_healthinfo/iolatency/logp

    chown root system /proc/tcl_healthinfo/memory
    chown root system /proc/tcl_healthinfo/memory/alloc_wait_h_ms
    chown root system /proc/tcl_healthinfo/memory/alloc_wait_l_ms
    chown root system /proc/tcl_healthinfo/memory/alloc_wait_logp_ms
    chown root system /proc/tcl_healthinfo/memory/cma_alloc_type
    chown root system /proc/tcl_healthinfo/memory/ion_wait_h_ms
    chown root system /proc/tcl_healthinfo/memory/ion_wait_l_ms
    chown root system /proc/tcl_healthinfo/memory/ion_wait_logp_ms
    chown root system /proc/tcl_healthinfo/memory/ctrl
    chown root system /proc/tcl_healthinfo/memory/logp
    # endif /* VENDOR_EDIT */

on post-fs-data
    # Create the directories used by the Wireless subsystem
    #mkdir /data/misc/wifi 0770 wifi wifi
    #mkdir /data/misc/wifi/sockets 0770 wifi wifi
    #mkdir /data/misc/dhcp 0770 dhcp system

    # Create directory used by audio subsystem
    #mkdir /data/misc/audio 0770 audio audio

    #mkdir /data/system 0775 system system

    setprop vold.post_fs_data_done 1

    #setprop init.post_fs_data.bootreason ${ro.boot.bootreason}

    # itux
    # Set this property to enable Thermal service
    setprop persist.service.thermal 1
    # Properties for Thermal Service
    setprop persist.thermal.turbo.dynamic 1
    setprop ro.thermal.ituxversion 3.0
    setprop persist.thermal.shutdown.msg 0
    setprop persist.thermal.shutdown.vibra 0
    setprop persist.thermal.shutdown.tone 0
    chown system system /sys/module/intel_mid_osip/parameters/force_shutdown_occured
    chown system system /sys/module/intel_mid_osip/parameters/thermal_shutdown_occured
    chown system system /sys/devices/platform/coretemp.0/temp2_threshold1
    chown system system /sys/devices/platform/coretemp.0/temp2_threshold2
    chown system system /sys/devices/virtual/thermal/thermal_zone6/emul_temp
    chown system system /sys/devices/virtual/thermal/thermal_zone7/emul_temp

    # Stop led blinking and reduce brightness
    write /sys/class/leds/white/device/led_lighting_effect 1
    write /sys/class/leds/white/brightness 0

    # Playready
    #mkdir /data/mediadrm/playready/ 0770 mediadrm mediadrm
    #copy  /system/vendor/firmware/PR-ModelCert /data/mediadrm/playready/bgroupcert.dat
    #chown mediadrm mediadrm /data/mediadrm/playready/bgroupcert.dat
    #chmod 0440 /data/mediadrm/playready/bgroupcert.dat

    # realtek saved data directory
    mkdir /data/vendor/realtek 0770 system media
    mkdir /data/vendor/realtek/databases 0770 media media
    mkdir /data/vendor/realtek/satellite 0770 media media

    #tcl save data directory
    mkdir /data/vendor/tcl 0770
    chown system system /data/vendor/tcl
    chown system system /data/vendor/tcl/c4p
    chmod 0770 /data/vendor/tcl/c4p

    #tcl oad data directory
    mkdir /data/vendor/upgrade 0770
    chown root system /data/vendor/upgrade
    #tcl change /persist minimum permissions
    chmod 0770 /persist
    chown system system  /persist
    chmod 0550  /persist/ua
    chmod 0550  /persist/crt
    chmod 0550  /persist/crt/client
    chown system system /persist/ua/
    chown system system /persist/crt/
    chown system system /persist/crt/client
    chown system system /persist/ua/hbbtv_UA.xml
    chown system system /persist/crt/ca-certificates.crt
    chown system system /persist/crt/client/client_cert_config.xml
    chown system system /persist/crt/client/intermediate.p12
    chown system system /persist/crt/client/mds_test.xml
    chown system system /persist/crt/client/www.tcl.com.cert_bag.pem
    chown system system /persist/crt/client/www.tcl.com.key.pem
    chown system system /persist/crt/client/fvp_c
    chown system system /persist/crt/client/fvp_k

    chmod 0550 /persist/ua/hbbtv_UA.xml
    chmod 0550 /persist/crt/ca-certificates.crt
    chmod 0550 /persist/crt/client/client_cert_config.xml
    chmod 0550 /persist/crt/client/intermediate.p12
    chmod 0550 /persist/crt/client/mds_test.xml
    chmod 0550 /persist/crt/client/www.tcl.com.cert_bag.pem
    chmod 0550 /persist/crt/client/www.tcl.com.key.pem
    chmod 0550 /persist/crt/client/fvp_c
    chmod 0550 /persist/crt/client/fvp_k
    # add end

    mkdir /var/TerminalManager  0770
#on property:init.post_fs_data.bootreason=panic
#    # Create dump dir and collect dumps. (use the same location as in init.rc)
#    mkdir /data/dontpanic 0750 root log

#    copy /proc/emmc_ipanic_console /data/dontpanic/ipanic_console
#    chown root log /data/dontpanic/ipanic_console
#    chmod 0640 /data/dontpanic/ipanic_console

#    copy /proc/emmc_ipanic_threads /data/dontpanic/ipanic_threads
#    chown root log /data/dontpanic/ipanic_threads
#    chmod 0640 /data/dontpanic/ipanic_threads

#    copy /proc/emmc_ipanic_gbuffer /data/dontpanic/ipanic_gbuffer
#    chown root log /data/dontpanic/ipanic_gbuffer
#    chmod 0640 /data/dontpanic/ipanic_gbuffer

#    # Clear panic partition
#    write /proc/emmc_ipanic_header 1

#chown system /dev/tshmem
service pvrsrvctl /vendor/bin/pvrsrvctl --start
    class core
    user root
    group root
    oneshot
    disabled
    seclabel u:r:vendor_modprobe:s0

service rtkd /vendor/bin/rtkd
    class    core
    user     root
    group    root system media media_rw everybody
    oneshot
    socket rtkd seqpacket 0660 system system
    writepid /dev/cpuset/foreground/tasks
	
service rtdlog /vendor/bin/rtdlog	
    class    core	
    user     root	
    group    root system media media_rw everybody	
    socket rtdlog seqpacket 0660 system system
    priority 10
    ioprio be 7
    writepid /dev/cpuset/system-background/tasks

service icVerifyd /system/bin/icVerifyd
    class    main
    oneshot

# bugreport is triggered by holding down volume down, volume up and power
service bugreport /system/bin/dumpstate -d -p -B -z \
        -o /data/user_de/0/com.android.shell/files/bugreports/bugreport
    class main
    disabled
    oneshot
    keycodes 114 115 116

# interval:60s margin:20s
service watchdogd /sbin/watchdogd 60 20
    class core
    oneshot
    seclabel u:r:watchdogd:s0


#add for set ro.oem.key1
service setoemkey1 /product/bin/oemkey1
	class main
	user root
	group root
	oneshot
	seclabel u:r:vendor_init:s0
   
# Reset the watchdog counter once boot is completed
on property:sys.boot_completed=1
    write /sys/devices/virtual/misc/watchdog/counter "0"
    write /sys/block/dm-0/queue/read_ahead_kb 256

# Enable native bridge for target executables
on early-init
    mount binfmt_misc binfmt_misc /proc/sys/fs/binfmt_misc

on property:ro.enable.native.bridge.exec=1
    copy /system/etc/binfmt_misc/arm_exe /proc/sys/fs/binfmt_misc/register
    copy /system/etc/binfmt_misc/arm_dyn /proc/sys/fs/binfmt_misc/register

on property:sys.boot_completed=1
    swapon_all /vendor/etc/fstab.${ro.hardware}

    #release boot logo memory
    write /sys/realtek_boards/reclaim_dvr 1
    write /sys/realtek_boards/reclaim_dvr 2

    #disable console dmesg
    write /proc/sys/kernel/printk "0 4 1 7"

    #enable more key
    #write /sys/venus_ir/ir_is_upload_more_key 1


